using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCFinalTask.Pages
{
    class HomePage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//div[contains(@id, 'links')]//a[text()='News']")]
        private IWebElement newsButton;

        public HomePage(IWebDriver driver) : base(driver) { }

        public void ClickOnNewsButton()
        {
            newsButton.Click();
        }
    }
}
