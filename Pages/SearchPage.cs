using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBCFinalTask.Pages
{
    class SearchPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//li/div[contains(@class, 'Promo')]//span[@aria-hidden]")]
        private IReadOnlyCollection<IWebElement> searchArticleResults;

        public SearchPage(IWebDriver driver) : base(driver) { }

        public string GetArticleTitleFromSearchPage()
        {
            return searchArticleResults.ElementAt(0).Text;
        }
    }
}
