using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCFinalTask.Pages
{
    class CoronavirusPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'block@m')]//span[text()='Your Coronavirus Stories']")]
        private IWebElement yourCoronavirusStoriesTab;

        public CoronavirusPage(IWebDriver driver) : base(driver) { }

        public void ClickOnYourCoronavirusStorieTab()
        {
            yourCoronavirusStoriesTab.Click();
        }
    }
}
