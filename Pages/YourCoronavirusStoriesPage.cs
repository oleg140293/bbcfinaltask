using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBCFinalTask.Pages
{
    class YourCoronavirusStoriesPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//div[contains(@aria-labelledby, 'Getintouchwithus')]//a")]
        private IReadOnlyCollection<IWebElement> getInTouchTabs;

        public YourCoronavirusStoriesPage(IWebDriver driver) : base(driver) { }

        public void ClickOnShareStoryTab()
        {
            getInTouchTabs.ElementAt(0).Click();
        }
    }
}
