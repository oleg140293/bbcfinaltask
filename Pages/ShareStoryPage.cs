using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCFinalTask.Pages
{
    class ShareStoryPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//textarea")]
        private IWebElement storyTextArea;

        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Name']")]
        private IWebElement nameInput;

        [FindsBy(How = How.XPath, Using = "//input[contains(@aria-label, 'Email')]")]
        private IWebElement emailInput;

        [FindsBy(How = How.XPath, Using = "//input[contains(@aria-label, 'Contact')]")]
        private IWebElement contactInput;

        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Location ")]
        private IWebElement locationInput;

        [FindsBy(How = How.XPath, Using = "//input[@type='checkbox']")]
        private IReadOnlyCollection<IWebElement> checkboxes;

        [FindsBy(How = How.XPath, Using = "//button[text()='Submit']")]
        private IWebElement submitButton;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'error-message')]")]
        private IWebElement cantPublishStoryMessage;

        public ShareStoryPage(IWebDriver driver) : base(driver) { }

        public void FillForm(string story, string name, string email, string contact, string location)
        {
            storyTextArea.SendKeys(story);
            nameInput.SendKeys(name);
            emailInput.SendKeys(email);
            contactInput.SendKeys(contact);
            locationInput.SendKeys(location);
        }

        public void SetValueOfCheckboxes()
        {
            foreach (IWebElement checkox in checkboxes)
                checkox.Click();
        }

        public void ClickSubmitButton()
        {
            submitButton.Click();
        }

        public string GetErrorMessageText()
        {
            return cantPublishStoryMessage.Text;
        }
    }
}
