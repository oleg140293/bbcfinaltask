using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BBCFinalTask.Pages
{
    class NewsPage : BasePage
    {
        //private readonly string expectedArticleHeadline = "In pictures: Floods kill dozens in Germany and Belgium";
        //private readonly string expectedSecondaryArticleTitle = "Some local counties have introduced strict rules in a bid to increase their vaccination rates.";
        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'none ')]//h3[contains(text(), 'Europe flood')]")]
        private IWebElement articleHeadline;

        [FindsBy(How = How.XPath, Using = "//p[contains(text(), 'Tokyo')]")]
        private IWebElement secondaryArticleTitle;

        [FindsBy(How = How.XPath, Using = "//input[@id='orb-search-q']")]
        private IWebElement searchInputBar;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'navigation')]//span[text()='Coronavirus']")]
        private IWebElement coronavirusTab;

        public NewsPage(IWebDriver driver) : base(driver) { }

        public string GetArticleHeadlineText()
        {
            return articleHeadline.Text;
        }

        public string GetSecondaryArticleTitleText()
        {
            return secondaryArticleTitle.Text;
        }

        public void EnterTextToSearchInputBar(string keyword)
        {
            searchInputBar.SendKeys(keyword);
            searchInputBar.SendKeys(Keys.Enter);
        }

        public void ClickOnCoronavirusTab()
        {
            coronavirusTab.Click();
        }
    }
}
