using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCFinalTask
{
    public class BBCStepsTask1Task2
    {
        private IWebDriver driver;

        private readonly string websiteURL = "https://www.bbc.com";

        private readonly int WAIT_TIME = 30;

        private readonly string newsTab = "//div[contains(@id, 'links')]//a[text()='News']";

        private readonly string articleHeadline = "//div[contains(@class, 'none ')]//h3[contains(text(), 'Europe flood')]";

        private readonly string secondaryArticleTitle = "//p[contains(text(), 'Tokyo')]";

        private readonly string searchInput = "//input[@id='orb-search-q']";
        private readonly string searchArticleResults = "//li/div[contains(@class, 'Promo')]//span[@aria-hidden]";

        private readonly string coronavirusTab = "//div[contains(@class, 'navigation')]//span[text()='Coronavirus']";
        private readonly string yourCoronavirusStoriesTab = "//div[contains(@class, 'block@m')]//span[text()='Your Coronavirus Stories']";
        private readonly string skipRegistrationButton = "//button[@aria-label='Close']";
        private readonly string sharingStoryTab = "//div[contains(@aria-labelledby, 'Getintouchwithus')]//a";
        private readonly string storyTextArea = "//textarea";
        private readonly string nameInput = "//input[@aria-label='Name']";
        private readonly string emailInput = "//input[contains(@aria-label, 'Email')]";
        private readonly string contactInput = "//input[contains(@aria-label, 'Contact')]";
        private readonly string locationInput = "//input[@aria-label='Location ']";
        private readonly string notPublishNameButton = "//p[contains(text(), 'Please')]";
        private readonly string acceptanceOfTermsButton = "//p[contains(text(), 'accept')]";
        private readonly string submitButton = "//button[text()='Submit']";
        private readonly string errorMessage = "//div[contains(@class, 'error-message')]";

        private readonly string expectedArticleHeadline = "Europe flood victims face massive clean-up";
        private readonly string expectedSecondaryArticleTitle =
            "Two athletes become the first to test positive for coronavirus at the Tokyo Olympic athletes' village - five days before the start of the Games.";
        private readonly string expectedArticleOnSearchPage = "Europe floods: Victims face massive clean-up as waters recede";
        private readonly string expectedErrorMessage = "can't be blank";
        private readonly string searchKeyword = "victims face";

        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl(websiteURL);
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void CheckHeadlineArticle()
        {
            IWebElement news = driver.FindElement(By.XPath(newsTab));
            news.Click();

            IWebElement articleTitle = driver.FindElement(By.XPath(articleHeadline));
            string headlineText = articleTitle.Text;
            Assert.AreEqual(headlineText, expectedArticleHeadline);
        }

        [Test]
        public void CheckSecondaryArticleTitle()
        {
            IWebElement news = driver.FindElement(By.XPath(newsTab));
            news.Click();

            IWebElement secondaryArticle = driver.FindElement(By.XPath(secondaryArticleTitle));
            string secondaryTitle = secondaryArticle.Text;
            Assert.AreEqual(secondaryTitle, expectedSecondaryArticleTitle);
        }

        [Test]
        public void CheckFirstArticleTitleOnSearchPage()
        {
            IWebElement news = driver.FindElement(By.XPath(newsTab));
            news.Click();

            WaitTime();
            IWebElement buttonSkipRegistration = driver.FindElement(By.XPath(skipRegistrationButton));
            buttonSkipRegistration.Click();

            IWebElement article = driver.FindElement(By.XPath(articleHeadline));

            IWebElement searchInputField = driver.FindElement(By.XPath(searchInput));
            searchInputField.SendKeys(searchKeyword);
            searchInputField.SendKeys(Keys.Enter);

            IReadOnlyCollection<IWebElement> articleTitles = driver.FindElements(By.XPath(searchArticleResults));
            string firstTitle = articleTitles.ElementAt(0).Text;
            Assert.AreEqual(firstTitle, expectedArticleOnSearchPage);
        }

        [Test]
        public void FormFillingTest()
        {
            IWebElement news = driver.FindElement(By.XPath(newsTab));
            news.Click();

            IWebElement coronavirusNews = driver.FindElement(By.XPath(coronavirusTab));
            coronavirusNews.Click();

            IWebElement yourCoronavirusStories = driver.FindElement(By.XPath(yourCoronavirusStoriesTab));
            yourCoronavirusStories.Click();

            WaitTime();
            IWebElement buttonSkipRegistration = driver.FindElement(By.XPath(skipRegistrationButton));
            buttonSkipRegistration.Click();

            IReadOnlyCollection<IWebElement> tabs = driver.FindElements(By.XPath(sharingStoryTab));
            tabs.ElementAt(0).Click();

            IWebElement storyTextField = driver.FindElement(By.XPath(storyTextArea));
            storyTextField.SendKeys("");

            IWebElement nameField = driver.FindElement(By.XPath(nameInput));
            nameField.SendKeys("Ivan");

            IWebElement emailField = driver.FindElement(By.XPath(emailInput));
            emailField.SendKeys("test@gmail.com");

            IWebElement contactField = driver.FindElement(By.XPath(contactInput));
            contactField.SendKeys("+012345678901");

            IWebElement locationField = driver.FindElement(By.XPath(locationInput));
            locationField.SendKeys("China");

            IWebElement buttonNotPublishName = driver.FindElement(By.XPath(notPublishNameButton));
            buttonNotPublishName.Click();

            IWebElement buttonAcceptanceOfTerms = driver.FindElement(By.XPath(acceptanceOfTermsButton));
            buttonAcceptanceOfTerms.Click();

            IWebElement buttonSubmit = driver.FindElement(By.XPath(submitButton));
            buttonSubmit.Click();

            IWebElement errorMessageField = driver.FindElement(By.XPath(errorMessage));
            string textAboutError = errorMessageField.Text;
            Assert.AreEqual(expectedErrorMessage, textAboutError);
        }

        public void WaitTime()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(WAIT_TIME);
        }


        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}