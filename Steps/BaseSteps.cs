using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using System;
using TechTalk.SpecFlow;


namespace BBCFinalTask.Steps
{
    class BaseSteps
    {
        private static IWebDriver driver;
        private readonly string URL = "https://www.bbc.com";
        private readonly int WAIT_TIME = 30;

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='Close']")]
        private IWebElement skipRegistrationButton;
    
        [Before]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl(URL);
            driver.Manage().Window.Maximize();
        }

        [After]
        public void TearDown()
        {
            driver.Quit();
        }

        public void WaitTime()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(WAIT_TIME);
        }

        public void SkipRegistration()
        {
            skipRegistrationButton.Click();
        }
    }
}
