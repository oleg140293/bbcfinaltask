using BBCFinalTask.Steps;
using BBCFinalTask.Pages;
using System;
using TechTalk.SpecFlow;
using NUnit.Framework;

namespace BBCFinalTask.Features
{
    [Binding]
    public class BBCFeatureSteps 
    {
        private BaseSteps baseSteps;
        private HomePage homePage;
        private NewsPage newsPage;
        private SearchPage searchPage;
        private CoronavirusPage coronavirusPage;
        private YourCoronavirusStoriesPage yourCoronavirusStoriesPage;
        private ShareStoryPage shareStoryPage;

        private readonly string headlineArticle;

        [When(@"User clicks News tab")]
        public void WhenUserClicksNewsTab()
        {
            homePage.ClickOnNewsButton();
        }
        
        [When(@"User makes search by keyword (.*)")]
        public void WhenUserMakesSearchByKeyword(string keyword)
        {
            newsPage.EnterTextToSearchInputBar(keyword);
        }
        
        [When(@"User clicks Coronavirus tab")]
        public void WhenUserClicksCoronavirusTab()
        {
            newsPage.ClickOnCoronavirusTab();
        }
        
        [When(@"User clicks Your Coronavirus Stories tab")]
        public void WhenUserClicksYourCoronavirusStoriesTab()
        {
            coronavirusPage.ClickOnYourCoronavirusStorieTab();
        }
        
        [When(@"User clicks Share Story tab")]
        public void WhenUserClicksShareStoryTab()
        {
            baseSteps.WaitTime();
            baseSteps.SkipRegistration();
            yourCoronavirusStoriesPage.ClickOnShareStoryTab();
        }
        
        [When(@"User enters value to the form (.*) (.*) (.*) (.*) (.*)")]
        public void WhenUserEntersValueToTheForm(string story, string name, string email, string conatct, string location)
        {
            shareStoryPage.FillForm(story, name, email, conatct, location);
            shareStoryPage.SetValueOfCheckboxes();
            shareStoryPage.ClickSubmitButton();
        }
        
        [Then(@"User checks that headline article is '(.*)'")]
        public void ThenUserChecksThatHeadlineArticleIs(string expectedHeadlineArticle)
        {
            Assert.AreEqual(newsPage.GetArticleHeadlineText(), expectedHeadlineArticle);
        }
        
        [Then(@"User checks that secondary article title is (.*)")]
        public void ThenUserChecksThatSecondaryArticleTitleIs(string expectedSecondaryArticleTitle)
        {
            Assert.AreEqual(newsPage.GetSecondaryArticleTitleText(), expectedSecondaryArticleTitle);
        }
        
        [Then(@"User checks that first found article is (.*)")]
        public void ThenUserChecksThatFirstFoundArticleIs(string expectedFoundArticle)
        {
            Assert.AreEqual(searchPage.GetArticleTitleFromSearchPage(), expectedFoundArticle);
        }
        
        [Then(@"User checks that error message is (.*)")]
        public void ThenUserChecksThatErrorMessageIs(string expectedErrorMessage)
        {
            Assert.AreEqual(shareStoryPage.GetErrorMessageText(), expectedErrorMessage);
        }
    }
}
