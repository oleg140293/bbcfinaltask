﻿Feature: BBCFeature
	As a user
	User want to test all main site functionality
	So that user can be sure that site works correctly

@positiveTest
Scenario: Check that main article header equals to expected
	When User clicks News tab
	Then User checks that headline article is 'Europe flood victims face massive clean-up'

@positiveTest
Scenario: Check that secondary article title equals to expected
	When User clicks News tab
	Then User checks that secondary article title is 'Two athletes become the first to test positive for coronavirus at the Tokyo Olympic athletes' village - five days before the start of the Games.'                   |

@positiveTest
Scenario: Check that first article header on NewsPage equals to expected on SearchPage
	When User clicks News tab
	And User makes search by keyword 'victims face'
	Then User checks that first found article is 'Europe floods: Victims face massive clean-up as waters recede'

@negativeTest
Scenario: Send story form with some kind of empty input field
	When User clicks News tab
	And User clicks Coronavirus tab
	And User clicks Your Coronavirus Stories tab
	And User clicks Share Story tab
	When User enters value to the form <Story> <Name> <Email> <Contact> <Location>
	Then User checks that error message is <Message>

Scenarios: 
	| Story | Name | Email          | Contact       | Location | Message             |
	|       | Ivan | test@gmail.com | +012345678901 | China    |  can't be blank     |
	| Story |      | test@gmail.com | +012345678901 | China    | Name can't be blank |